FROM python:3.6-slim-stretch
RUN apt update
RUN apt install -y gcc
RUN pip install pip Cython Pillow==5.0.0
RUN pip install numpy torch torchvision
RUN pip install flask 
COPY app app/
EXPOSE  8000
CMD ["python", "app/server.py"]
