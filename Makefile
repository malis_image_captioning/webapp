REPO_NAME = malisapp
CONTAINER_PORT = 8000
HOST_PORT = 5000

build: 
	-docker stop malisapp
	-docker container rm malisapp
	docker image rm -f $(REPO_NAME)
	docker build --rm=false -t $(REPO_NAME) .
	docker run --rm -p $(HOST_PORT):$(CONTAINER_PORT) $(REPO_NAME)
