import torch
from torch import nn
from torchvision import models
class Encoder(nn.Module):
    def __init__(self, embed_size):
        #call the constrctor of mother-class
        super(Encoder, self).__init__()
        
        #load pretrained resnet(110 layers)
        self.resnet101 = models.resnet101(pretrained=False)
        last_layer_nodes = self.resnet101.fc.in_features
        self.resnet101 = nn.Sequential(*(list(self.resnet101.children())[:-1]))
        
        #freeze weights
        for parameter in self.resnet101.parameters():
            parameter.requires_grad = False
            
        #replace last fully connected layer(trainable layer)
        self.fc = nn.Linear(last_layer_nodes, embed_size)
        
        #add a batch normalization layer after each batch
        self.batch_norm = nn.BatchNorm1d(embed_size, momentum=0.01)
        
    def forward(self, batch):
        #
        features = self.resnet101(batch)
        features = features.reshape(features.size(0), -1)
        features = self.batch_norm(self.fc(features))
        return features

class Decoder(nn.Module):
    def __init__(self, embed_size, hidden_size, vocab_size, num_layers, max_seq_length):
        #call the super class's constructor
        super(Decoder, self).__init__()

        #create the embedding layer
        self.embed = nn.Embedding(vocab_size, embed_size)

        #create the LSTM cell
        self.lstm = nn.LSTM(embed_size, hidden_size, num_layers, batch_first=True)

        #create the linear outer layer
        self.linear = nn.Linear(hidden_size, vocab_size)

        #maximum sequence length
        self.max_seg_length = max_seq_length
        
    def forward(self, features, captions, lengths):
        embeddings = self.embed(captions)
        embeddings = torch.cat((features.unsqueeze(1), embeddings), 1)
        packed = pack_padded_sequence(embeddings, lengths, batch_first=True) 
        hiddens, _ = self.lstm(packed)
        outputs = self.linear(hiddens[0])
        return outputs
    def predict(self, features, states=None):
        predictedids = []
        inputs = features.unsqueeze(1)
        #while number of generated words is smaller than max_seg_length
        for i in range(self.max_seg_length):
            #forward the input through the lstm
            hiddens, states = self.lstm(inputs, states)
            #map to the vocabulary size
            outputs = self.linear(hiddens.squeeze(1))
            _, predicted = outputs.max(1)
            predictedids.append(predicted)
            inputs = self.embed(predicted)
            inputs = inputs.unsqueeze(1)
        predictedids = torch.stack(predictedids, 1)
        return predictedids
        
class Vocabulary():
    def __init__(self):
        self.word_to_id = {}
        self.id_to_word = {}
        self.id = 0

    def append(self, word):
        if not word in self.word_to_id:
            self.word_to_id[word] = self.id
            self.id_to_word[self.id] = word
            self.id += 1

    def __call__(self, word):
        if not word in self.word_to_id:
            return self.word_to_id['<NA>']
        return self.word_to_id[word]

    def __len__(self):
        return self.id+1