$(document).ready(function(){
    $("form").submit(function(e){
        e.preventDefault(e);
    });
    $(".fileinput-upload").removeAttr("type");
    $(".fileinput-upload").attr("id","upload_button");
    $(".fileinput-upload").on('click', function() { 
        console.log("i'm here1")
        $(".spinner-grow").show()
        var form = document.getElementById('form');
        var formData = new FormData(form);
        var xhr = new XMLHttpRequest();
        // Add any event handlers here...
        xhr.open('POST', '/upload', true);
        xhr.send(formData);
        xhr.onload  = function() {
            console.log(xhr.responseText);
            var jsonResponse = JSON.parse(xhr.responseText);
            $(".file-caption-info").css({'font-weight' : 'bold', 'color' : 'green',
                                         'white-space' : 'unset', 'height' : 'unset'})
            $(".kv-preview-thumb").each(function(index){
                thumb = $(this)
                $(thumb).find(".file-caption-info").text(jsonResponse[index])
                $(thumb).find(".file-size-info").remove()
                $(".spinner-grow").hide(50)
            })
         };
    }); 
    
})
