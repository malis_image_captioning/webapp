import numpy as np
import torch
from torch import nn
import torch.nn.functional as F
from torchvision import transforms
from PIL import Image
import classes
import pickle

embed_size = 256
in_size = 224
hidden_size = 512
num_layers = 1
max_seq_length = 20

transform = transforms.Compose([ 
        transforms.Resize((in_size,in_size)),
        transforms.ToTensor(), 
        transforms.Normalize((0.485, 0.456, 0.406), 
                            (0.229, 0.224, 0.225))])
def loadAll():
    with open("app/model/vocab", 'rb') as f:
        vocab = pickle.load(f)

    encoder = torch.load("app/model/encoder.pt",map_location=torch.device('cpu'))
    decoder = torch.load("app/model/decoder.pt",map_location=torch.device('cpu'))
    encoder.eval()
    decoder.eval()
    return encoder, decoder, vocab

def getCaption(image,encoder,decoder,vocab):
    image = image.convert('RGB')
    image = transform(image).unsqueeze(0)
    features = encoder(image)
    predictedids = decoder.predict(features)
    predictedids = predictedids[0].cpu().numpy()
    predicted_caption = []
    for word_id in predictedids:
        word = vocab.id_to_word[word_id]
        predicted_caption.append(word)
        if word == '<end>': break
    sentence = ' '.join(predicted_caption[1:-1])
    return sentence
