from flask import Flask, render_template, request, jsonify
import modelWrapper
from classes import Vocabulary, Encoder, Decoder
from modelWrapper import loadAll, getCaption
import pickle
from PIL import Image
from io import BytesIO
encoder, decoder, vocab = loadAll()

# setting  the server up
app = Flask(__name__,static_url_path='')

#routing point at POST /upload
@app.route("/upload", methods=['GET', 'POST'])
def upload():
    uploaded_files = request.files.getlist("file")
    captions = []
    for file in uploaded_files:
        image = Image.open(BytesIO(file.read()))
        caption = getCaption(image,encoder,decoder,vocab)
        captions.append(caption)
    return jsonify(captions)

#routing point at the index
@app.route("/")
def index():
    return render_template("index.html")

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, debug=False)
